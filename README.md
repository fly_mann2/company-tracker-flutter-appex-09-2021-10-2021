# README #
---

## Website
https://company-tracker-6342f.firebaseapp.com/#/ 
Feil: https://company-tracker-6342f.web.app/#/ 

### Login
Logg inn som gjest med å trykke på (logg inn) knappen, eller logg inn med google brukeren din.

## Hjerte, Info, Kommentar
### Hjerte Icon.
1. Legg til/fjern favoritter.

### Info Icon.
1. Henter regnskap verdier hvis tilgjengelig. 
Eksempel: Organisasjonsnummer: 989061593, Navn: -G-group as

### Kommentar Icon.
1. Viser (Textfield) og (Lagre knapp) 
2.  (Textfield) skriv kommentar.
3.  (Lagre knapp) lagrer kommentaren i (Textfield).

### Om
1. Webapplikasjon.
2. Hente oppslag-data om bedrifter fra Brønnøysundregisteret. 
3. Lagre potensielle kunder basert på oppslag-data om bedrifter fra Brønnøysundregisteret.
4. Brukeren kan registrere-lagre ekstra info om hver bedrift.
5. Hente data fra (1-Flere) Api'er om samme bedrift basert på "org.nr./postnr".

## Kode
Koden ligger i Flutter_appex/lib.
For API som ikke har CORS enable bruker jeg dette prosjektet.
Ikke mitt. Det bruker Node.js og Heroku.
https://github.com/Rob--W/cors-anywhere 



### Android
!Android versjon støtter ikke CORS. Info knappen viser alltid "Ingen" på Android.

![](images/mobile/Mobile.PNG)
![](images/mobile/mobileLogin.PNG)

### Sketch-design
Path: images/sketch-design
### Installasjon
Install Flutter: https://flutter.dev/docs/get-started/install/windows


### Antall timer
2021 Sep 6-21. ca 2-3 uker.
12 dager.
5 timer per dag.
25 timer pr uke.

Totalt 60 timer.

Tid brukt: 41 timer.

### API
989061593 = organisasjonsnummer.
Brreg Regnskap:      data.brreg.no/regnskapsregisteret/regnskap/989061593
Brreg Enheter :      data.brreg.no/enhetsregisteret/api/enheter

### Gantt diagram
![](images/charts/Gantt_Chart.PNG)
### Branch structure
Prototype (1-Flere) → Master (1-1) → Version (1-Flere)

### Framgang prosess
	Notion(Gantt chart: project timeline) ->
	
		Notion(Scrum board: backlog) ->
		
			Google calendar(Every sprint: Plan week) ->
			
				Toggl(Tracktime)

## Vertøy
--- 

### Utvikling
Software development kit (SDK)/Frontend: Flutter

Programming language: Dart

Development Environment (IDE)/Emulator: Android studio

Code editor: VsCode

Baas(Backend-as-a-Service): Firebase

API debug: Postmann

(New) API enable CORS: node.js og Heroku

### Planlegging
Notion(Gantt chart, Scrum board)

Google calendar(Week plan)

Toggl(Time tracking)

## Toggl
Tid brukt: 41 timer.

![](images/toggl/toggl1.PNG)

![](images/toggl/toggl2.PNG)

![](images/toggl/toggl3.PNG)



