import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_demo/api/company_json.dart';
import 'package:flutter_demo/firebase/firestore_path.dart';
import 'package:flutter_demo/pages/more_info_page.dart';

class CompanyUICard extends StatefulWidget {
  const CompanyUICard({Key? key, required this.company}) : super(key: key);
  final Company company;

  @override
  State<CompanyUICard> createState() => _CompanyUICardState();
}

// Expand/Hide state
enum StateExpand {
  hide,
  info,
  comment,
}

class _CompanyUICardState extends State<CompanyUICard> {
  StateExpand curState = StateExpand.hide;
  final _textEditingController = TextEditingController();
  var screenSpace = 0.3;
  var iconScreenSpace = 0.06;

  @override
  Widget build(BuildContext context) {
    // Get current screen size
    var screenSize = MediaQuery.of(context).size.width;
    // Change between fullscreen* and mobile/tablet
    screenSize < 750
        ? {screenSpace = 0.95, iconScreenSpace = 0.2}
        : {
            // Width space for headers: 4 spaces, 2 for name
            screenSpace = (0.95 - (iconScreenSpace * 3)) / 4,
            // Width space for icons buttons
            iconScreenSpace = 0.06
          };

    return Container(
      width: screenSize,
      alignment: Alignment.centerLeft,
      child: Column(children: [
        /* Change CompanyUICard layout based on screen size.
           Big   = |title, icons|
          -----------------------
           Small = |title|
                   |icons|
        */
        Flex(
          direction: screenSize < 750 ? Axis.vertical : Axis.horizontal,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // organisasjonsnummer
            SizedBox(
                width: screenSize * screenSpace,
                child: SelectableText(
                    widget.company.companyBrreg.organisasjonsnummer,
                    style: const TextStyle(color: Colors.grey))),
            // navn
            SizedBox(
                width: screenSize * screenSpace * 2,
                child: Text(widget.company.companyBrreg.navn,
                    style: const TextStyle(color: Colors.grey))),
            // If exist: poststed/postnummer
            SizedBox(
                width: screenSize * screenSpace,
                child: Text(
                    widget.company.companyBrreg.poststed +
                        "/" +
                        widget.company.companyBrreg.postnummer,
                    style: const TextStyle(color: Colors.grey))),
            // Check if this is a title/filter card. No company info only headers
            (widget.company.companyBrreg.organisasjonsnummer == "Org.NR:")
                ? const SizedBox.shrink()
                : Row(
                    children: [
                      SizedBox(
                        width: screenSize * iconScreenSpace,
                        child: IconButton(
                          icon: Icon(Icons.favorite,
                              color: (widget.company.heart == true)
                                  ? Colors.white70
                                  : Colors.white10),
                          onPressed: () {
                            if (FirestorePath.isLoggedIn()) {
                              setState(() {
                                widget.company.heart = !widget.company.heart;
                                FirestorePath.setFavorite(widget.company);
                                FirestorePath.updateFavoritePage();
                              });
                            }
                          },
                        ),
                      ),
                      // Expand button: show more_info_page
                      SizedBox(
                        width: screenSize * iconScreenSpace,
                        child: IconButton(
                          icon: const Icon(Icons.info, color: Colors.grey),
                          onPressed: () {
                            setState(() {
                              curState != StateExpand.info
                                  ? curState = StateExpand.info
                                  : curState = StateExpand.hide;
                            });
                          },
                        ),
                      ),
                      // Expand button: write/post comment field
                      SizedBox(
                        width: screenSize * iconScreenSpace,
                        child: IconButton(
                          icon: Icon(Icons.add_comment,
                              color: (widget.company.notes.isNotEmpty)
                                  ? Colors.white70
                                  : Colors.white10),
                          onPressed: () {
                            if (FirestorePath.isLoggedIn()) {
                              setState(() {
                                curState != StateExpand.comment
                                    ? curState = StateExpand.comment
                                    : curState = StateExpand.hide;
                                _textEditingController.text =
                                    widget.company.notes;
                              });
                            }
                          },
                        ),
                      ),
                    ],
                  ),
          ],
        ),
        // Expand/Hide
        curState == StateExpand.hide
            // Hide:
            ? const SizedBox.shrink()
            : curState == StateExpand.info
                // Expand: more_info_Page
                ? SizedBox(
                    height: 50,
                    child: MoreInfoPage(
                        orgNR: widget.company.companyBrreg.organisasjonsnummer),
                  )
                : Row(
                    // Expand: write/post comment
                    children: [
                        // TextField for adding comment
                        SizedBox(
                            width: screenSize * (0.95 - iconScreenSpace),
                            height: 100,
                            child: Container(
                              color: Colors.white10,
                              child: TextField(
                                style: const TextStyle(color: Colors.white70),
                                decoration: const InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.red),
                                  ),
                                ),
                                controller: _textEditingController,
                                minLines: 5,
                                maxLines: 5,
                              ),
                            )),
                        // Post comment button
                        SizedBox(
                          width: screenSize * iconScreenSpace,
                          child: IconButton(
                            icon: const Icon(Icons.save, color: Colors.white70),
                            onPressed: () {
                              setState(() {
                                curState = StateExpand.hide;
                                widget.company.notes =
                                    _textEditingController.text;
                                FirestorePath.setNotes(widget.company);
                                FirestorePath.updateFrontPage();
                                FirestorePath.updateFavoritePage();
                              });
                            },
                          ),
                        )
                      ]),
      ]),
    );
  }
}
