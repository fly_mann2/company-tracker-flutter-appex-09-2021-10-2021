import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_demo/api/company_json.dart';
import 'package:flutter_demo/firebase/firestore_path.dart';

import 'company_ui_card.dart';

class CompanyUIList extends StatelessWidget {
  CompanyUIList(this.companys, this.isLoading, {Key? key}) : super(key: key);

  List<Company> companys = [];
  bool isLoading = true;
  // Display list
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xff1e1e24),
        body: isLoading
            ? const Text(
                "Ingen",
                style: TextStyle(color: Colors.white),
              )
            : Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  // Empty title/filter card
                  SizedBox(
                      child: SizedBox(
                          child: CompanyUICard(
                              company: FirestorePath.addEmptyCompany()))),
                  // Title line --------------
                  const SizedBox(
                    height: 20,
                    child: Divider(
                      height: 10,
                      thickness: 1,
                      color: Colors.grey,
                      endIndent: 50,
                      indent: 5,
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: companys.length,
                      itemBuilder: (context, index) {
                        return CompanyUICard(
                          company: companys[index],
                        );
                      },
                    ),
                  )
                ],
              ));
  }
}
