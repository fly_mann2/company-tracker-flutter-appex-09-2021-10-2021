import 'package:flutter/material.dart';
import 'package:flutter_demo/top_bar/custom_tab_bar.dart';
import 'package:flutter_demo/pages/favorite_page.dart';
import 'package:flutter_demo/pages/front_page.dart';
import '../top_bar/content_veiw.dart';
import '../top_bar/custom_tab.dart';
import 'login_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  var scaffoldKey = GlobalKey<ScaffoldState>();
  // remove late
  late double screenWidth;
  late double screenHeight;
  late double topPadding;
  late double bottomPadding;

  final IconData next = Icons.home;
  //remove late
  late TabController tabController;

  /* 
  Shows: Topbar select page  (login, favoritter, home)
         pages               (login, favoritter, home)
  */
  List<ContentView> contentViews = [
    // LoginPage
    ContentView(
        tab: const CustomTab(iconPic: Icon(Icons.login)),
        content: const Center(
          // Display page
          child: LoginPage(),
        )),
    // Fliter card, FavoritePage
    ContentView(
        tab: const CustomTab(iconPic: Icon(Icons.favorite)),
        content: const FavoritePage()),
    // Fliter card, FrontPage
    ContentView(
        tab: const CustomTab(iconPic: Icon(Icons.home)),
        content: const FrontPage()),
  ];
  // On page init
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: contentViews.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;
    topPadding = screenHeight * 0.05;
    bottomPadding = screenHeight * 0.01;

    return Scaffold(
      // BackgroundColor
      backgroundColor: const Color(0xff1e1e24),
      key: scaffoldKey,
      body: Container(
        child: Padding(
          padding:
              EdgeInsets.only(bottom: bottomPadding, top: topPadding, left: 20),
          child: LayoutBuilder(
            builder: (context, constraints) {
              // Switch between: big screen and small screen view
              if (constraints.maxWidth > 715) {
                return desktopView();
              } else {
                return mobileView();
              }
            },
          ),
        ),
      ),
    );
  }

// Switch to Desktop
  Widget desktopView() {
    return Column(
      // Alignment
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        // Switch between pages
        CustomTabBar(
          controller: tabController,
          tabs: contentViews.map((e) => e.tab).toList(),
        ),
        // Flexible: Removes bottom overflow error
        Flexible(
          // SizedBox: set width and heigt used by page
          child: SizedBox(
            height: screenHeight * 0.85,
            child: TabBarView(
              controller: tabController,
              children: contentViews.map((e) => e.content).toList(),
            ),
          ),
        ),
      ],
    );
  }

  /* 
  Switch to mobile view.
  For now: Same as Desktop view.
    Not implemented yet: sidepanel
  */
  Widget mobileView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        CustomTabBar(
          controller: tabController,
          tabs: contentViews.map((e) => e.tab).toList(),
        ),
        Flexible(
          child: SizedBox(
            height: screenHeight * 0.85,
            child: TabBarView(
              controller: tabController,
              children: contentViews.map((e) => e.content).toList(),
            ),
          ),
        ),
      ],
    );
  }
}
