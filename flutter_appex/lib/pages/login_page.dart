import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_demo/firebase/firestore_path.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // Background color
  final Color primaryColor = const Color(0xff1e1e24);

  final Color logoGreen = Colors.green;
  // Textfield. Username and Password
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  // Google sign in
  final GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  Future signInWithGoogle() async {
    final googleUser = await _googleSignIn.signIn();
    if (googleUser == null) return;

    final googleAuth = await googleUser.authentication;

    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    await FirebaseAuth.instance.signInWithCredential(credential);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // Current user
    final user = FirebaseAuth.instance.currentUser;

    return Scaffold(
        backgroundColor: primaryColor,
        body: Center(
          child: Container(
            width: 500,
            alignment: Alignment.topCenter,
            margin: const EdgeInsets.symmetric(horizontal: 30),
            child: SingleChildScrollView(
              child: user != null
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                          MaterialButton(
                            elevation: 0,
                            minWidth: double.maxFinite,
                            height: 50,
                            onPressed: () async {
                              await FirebaseAuth.instance.signOut();
                              // Update FrontPage and FavoritePage
                              setState(() {
                                FirestorePath.updateFrontPage();
                                FirestorePath.updateFavoritePage();
                              });
                            },
                            color: Colors.blue,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const <Widget>[
                                Icon(Icons.logout),
                                SizedBox(width: 10),
                                Text('Sign-out',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16)),
                              ],
                            ),
                            textColor: Colors.white,
                          ),
                          const SizedBox(height: 100),
                        ])
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        const Text(
                          'Logg inn',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 28),
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          //'Enter your email and password '
                          'Disable: Email and password '
                          '\nGjest bruker. Press login button',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        _buildTextField(nameController, Icons.account_circle,
                            'Brukernavn:'),
                        const SizedBox(height: 20),
                        _buildTextField(
                            passwordController, Icons.lock, 'Passord:'),
                        const SizedBox(height: 30),
                        MaterialButton(
                          elevation: 0,
                          minWidth: double.maxFinite,
                          height: 50,
                          onPressed: () async {
                            // Prefixed user. For login button
                            await signIn("test@hotmail.com", "Test123");

                            FirestorePath.updateFrontPage();
                            FirestorePath.updateFavoritePage();
                            setState(() {
                              if (FirestorePath.isLoggedIn()) {}
                            });
                          },
                          color: logoGreen,
                          child: const Text('Logg inn',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                          textColor: Colors.white,
                        ),
                        const SizedBox(height: 20),
                        MaterialButton(
                          elevation: 0,
                          minWidth: double.maxFinite,
                          height: 50,
                          onPressed: () async {
                            final googleUser = await _googleSignIn.signIn();
                            if (googleUser == null) return;

                            final googleAuth = await googleUser.authentication;

                            final credential = GoogleAuthProvider.credential(
                              accessToken: googleAuth.accessToken,
                              idToken: googleAuth.idToken,
                            );

                            await FirebaseAuth.instance
                                .signInWithCredential(credential);

                            setState(() {});
                          },
                          color: Colors.blue,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const <Widget>[
                              Icon(Icons.login),
                              SizedBox(width: 10),
                              Text('Logg inn med Google',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16)),
                            ],
                          ),
                          textColor: Colors.white,
                        ),
                        const SizedBox(height: 100),
                      ],
                    ),
            ),
          ),
        ));
  }
}

// TextFields for username and password
_buildTextField(
    TextEditingController controller, IconData icon, String labelText) {
  return Container(
    color: Colors.blueGrey,
    child: TextField(
      controller: controller,
      style: const TextStyle(color: Colors.white),
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.symmetric(horizontal: 10),
          labelText: labelText,
          labelStyle: const TextStyle(color: Colors.white),
          icon: Icon(
            icon,
            color: Colors.white,
          ),
          border: InputBorder.none),
    ),
  );
}

Future signIn(String _email, String _password) async {
  try {
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: _email, password: _password);
  } on FirebaseAuthException catch (e) {
    if (e.code == 'user-not-found') {
      debugPrint('No user found for that email.');
    } else if (e.code == 'wrong-password') {
      debugPrint('Wrong password provided for that user.');
    }
  }
}
