import 'package:flutter/material.dart';
import 'package:flutter_demo/api/company_api.dart';
import 'package:flutter_demo/api/company_brreg_json.dart';
import 'package:flutter_demo/api/company_json.dart';
import 'package:flutter_demo/custom_widgets/company_ui_list.dart';
import 'package:flutter_demo/firebase/firestore_path.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  State<FavoritePage> createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  List<Company> _companys = [];
  bool _isLoading = true;
  final TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    FirestorePath.favoritePageSetState = updateState;
    updateState();
  }

  // Update page
  Future updateState({String search = ""}) async {
    await getCompanys(search: search);
    setState(() {
      _isLoading = false;
    });
  }

  // Get all favorites from firebase
  Future<void> getCompanys({String search = ""}) async {
    _companys = [];
    _isLoading = true;

    await FirestorePath.getAllFavorites(_companys);
    for (var i in _companys) {
      List<CompanyBrreg> _companysBrreg = await CompanyApi.getApiFrontPage(
          companyNumber: i.companyBrreg.organisasjonsnummer);
      if (_companysBrreg.length == 1) {
        i.companyBrreg = _companysBrreg[0];
      }
    }
  }

  // Display list
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [Expanded(child: CompanyUIList(_companys, _isLoading))],
    );
  }
}
