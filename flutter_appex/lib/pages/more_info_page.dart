import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo/api/company_api.dart';
import 'package:flutter_demo/api/company_brreg_accounting_api.dart';

class MoreInfoPage extends StatefulWidget {
  const MoreInfoPage({Key? key, required this.orgNR}) : super(key: key);

  final String orgNR;

  @override
  State<MoreInfoPage> createState() => _MoreInfoPageState();
}

class _MoreInfoPageState extends State<MoreInfoPage> {
  bool _isLoading = true;
  bool _isNoValue = true;
  late CompanyAccountingBrreg companyAccountingBrreg;

  @override
  void initState() {
    super.initState();

    updateState();
  }

  Future updateState() async {
    await getMoreInfo();
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> getMoreInfo() async {
    List<CompanyAccountingBrreg> _companyAccountingBrregList =
        await CompanyApi.getCompanyAccounting(widget.orgNR);
    if (_companyAccountingBrregList.length == 1) {
      companyAccountingBrreg = _companyAccountingBrregList[0];
      _isNoValue = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white10,
      body: _isLoading
          ? const Text(
              "Laster inn",
              style: TextStyle(color: Colors.white70),
            )
          : (_isNoValue)
              ? const Text(
                  "Ingen",
                  style: TextStyle(color: Colors.white70),
                )
              // Json(Regnskap) Table
              : Table(
                  children: [
                    // regnskapsperiode
                    TableRow(children: [
                      const Text(
                        "regnskapsperiode:",
                        style: TextStyle(color: Colors.white70),
                      ),
                      Text(
                        companyAccountingBrreg.regnskapsperiode.fraDato +
                            " - " +
                            companyAccountingBrreg.regnskapsperiode.tilDato,
                        style: const TextStyle(color: Colors.white70),
                      ),
                    ]),
                    // valuta
                    TableRow(children: [
                      const Text(
                        "valuta: ",
                        style: TextStyle(color: Colors.white70),
                      ),
                      Text(companyAccountingBrreg.valuta,
                          style: const TextStyle(color: Colors.white70)),
                    ]),
                    // egenkapitalGjeld
                    TableRow(children: [
                      const Text(
                        "egenkapitalGjeld:",
                        style: TextStyle(color: Colors.white70),
                      ),
                      Text(
                        companyAccountingBrreg
                            .egenkapitalGjeld.sumEgenkapitalGjeld
                            .toString(),
                        style: const TextStyle(color: Colors.white70),
                      ),
                    ]),
                  ],
                ),
    );
  }
}
