import 'package:flutter/material.dart';
import 'package:flutter_demo/api/company_api.dart';
import 'package:flutter_demo/api/company_brreg_json.dart';
import 'package:flutter_demo/api/company_json.dart';
import 'package:flutter_demo/custom_widgets/company_ui_list.dart';
import 'package:flutter_demo/firebase/firestore_path.dart';

// remove
class FrontPage extends StatefulWidget {
  const FrontPage({Key? key}) : super(key: key);

  @override
  State<FrontPage> createState() => _FrontPageState();
}

class _FrontPageState extends State<FrontPage> {
  List<Company> _companys = [];
  bool _isLoading = true;
  final TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    FirestorePath.frontPageSetState = updateState;
    updateState();
  }

  Future updateState({String searchX = ""}) async {
    await getCompanys(search: searchX);
    setState(() {
      _isLoading = false;
    });
  }

  void searchAfterName() {
    if (searchController.text.isNotEmpty) {
      updateState(searchX: searchController.text);
    } else {
      updateState();
    }
  }

  // Get companyes from API
  Future<void> getCompanys({String search = ""}) async {
    _companys = [];
    List<CompanyBrreg> _companysBrreg = [];
    _companysBrreg = await CompanyApi.getApiFrontPage(companyName: search);

    for (var i in _companysBrreg) {
      _companys.add(Company(companyBrreg: i, heart: false, notes: ""));
    }
    await FirestorePath.getFrontPage(_companys);
  }

  // Display list
  @override
  Widget build(BuildContext context) {
    var screenSpace = 0.95;
    var iconScreenSpace = 0.06;
    var screenSize = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Row(
          // Empty space
          children: [
            // Search field
            SizedBox(
              height: 50,
              width: screenSize * (screenSpace - iconScreenSpace),
              child: Container(
                color: Colors.white10,
                child: TextField(
                  onSubmitted: (value) {
                    searchAfterName();
                  },
                  controller: searchController,
                  decoration: const InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                      labelText: "Søk på navn",
                      labelStyle: TextStyle(color: Colors.white54),
                      border: InputBorder.none),
                  style: const TextStyle(color: Colors.white70),
                ),
              ),
            ),
            // Search button
            SizedBox(
              width: screenSize * iconScreenSpace,
              child: IconButton(
                  icon: const Icon(Icons.search, color: Colors.grey),
                  onPressed: () {
                    searchAfterName();
                  }),
            ),
          ],
        ),
        Expanded(child: CompanyUIList(_companys, _isLoading))
      ],
    );
  }
}
