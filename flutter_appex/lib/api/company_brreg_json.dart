class CompanyBrreg {
  String organisasjonsnummer;
  String navn;
  String poststed;
  String postnummer;

  CompanyBrreg({
    required this.organisasjonsnummer,
    required this.navn,
    required this.poststed,
    required this.postnummer,
  });
  // "read" json
  factory CompanyBrreg.fromJson(dynamic json) {
    var forretningsadresse = (json["forretningsadresse"] == null)
        ? null
        : json["forretningsadresse"];
    return CompanyBrreg(
      //postadresse
      organisasjonsnummer: json['organisasjonsnummer'] as String,
      navn: json['navn'] as String,
      // forretningsadresse: Could be null
      poststed: (forretningsadresse == null)
          ? ""
          : (forretningsadresse["poststed"] == null)
              ? ""
              : forretningsadresse["poststed"],
      postnummer: (forretningsadresse == null)
          ? ""
          : (forretningsadresse["postnummer"] == null)
              ? ""
              : forretningsadresse["postnummer"],
    );
  }
  static List<CompanyBrreg> companyFromSnapshot(List snapshot) {
    return snapshot.map((data) {
      return CompanyBrreg.fromJson(data);
    }).toList();
  }
}
