import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter_demo/api/company_brreg_json.dart';
import 'package:http/http.dart' as http;

import 'company_brreg_accounting_api.dart';

class CompanyApi {
  // Brreg Enheter: data.brreg.no/enhetsregisteret/api/enheter
  static Future<List<CompanyBrreg>> getApiFrontPage(
      // Default values
      {String companyName = "",
      int numberOfEnheter = 100,
      String companyNumber = ""}) async {
    List _temp = [];

    try {
      Uri uri;
      if (companyName.isEmpty) {
        uri = Uri.https('data.brreg.no', '/enhetsregisteret/api/enheter', {
          "size": numberOfEnheter.toString(),
          "organisasjonsnummer": companyNumber,
        });
      } else {
        uri = Uri.https('data.brreg.no', '/enhetsregisteret/api/enheter', {
          "navn": companyName,
          "size": numberOfEnheter.toString(),
        });
      }

      final response =
          await http.get(uri, headers: {'Content-Type': 'application/json'});

      // norwegian characters
      var responsebody = utf8.decode(response.bodyBytes);
      Map data = json.decode(responsebody);
      for (var i in data['_embedded']['enheter']) {
        _temp.add(i);
      }
    } catch (e) {
      debugPrint(e.toString());
    }

    return CompanyBrreg.companyFromSnapshot(_temp);
  }

  /* Regnskap API: 'https://data.brreg.no/regnskapsregisteret/regnskap/'
  CORS not enabled for this API.
  Using server(cors-anywhere: node.js) on Heroku with CORS enabled.
  Server                                 API 
  "powerful-atoll-85365.herokuapp.com" + https://data.brreg.no
  */
  static Future<List<CompanyAccountingBrreg>> getCompanyAccounting(
      String companyNumber) async {
    List _temp = [];
    try {
      var uri = Uri.https(
        "powerful-atoll-85365.herokuapp.com",
        'https://data.brreg.no' '/regnskapsregisteret/regnskap/' +
            companyNumber,
      );
      // Allow acces from any orgin
      var response = await http.get(uri, headers: {
        'Access-Control-Allow-Origin': '*',
      });

      if (response.statusCode == 200) {
        var data = response.body;
        var responsebody = await json.decode(data);

        for (var i in responsebody) {
          _temp.add(i);
        }
      }
    } catch (e) {
      debugPrint(e.toString());
    }
    // foreach(element in array) fromJson
    return CompanyAccountingBrreg.companyFromSnapshot(_temp);
  }
}
