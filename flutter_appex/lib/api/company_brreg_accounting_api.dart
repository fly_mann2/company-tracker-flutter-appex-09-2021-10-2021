class Regnskapsperiode {
  Regnskapsperiode({
    required this.fraDato,
    required this.tilDato,
  });
  String fraDato;
  String tilDato;

  factory Regnskapsperiode.fromJson(Map<String, dynamic> json) =>
      Regnskapsperiode(
        fraDato: json["fraDato"],
        tilDato: json["tilDato"],
      );
}

class EgenkapitalGjeld {
  EgenkapitalGjeld({
    required this.sumEgenkapitalGjeld,
  });
  int sumEgenkapitalGjeld;

  factory EgenkapitalGjeld.fromJson(Map<String, dynamic> json) =>
      EgenkapitalGjeld(
        sumEgenkapitalGjeld: json["sumEgenkapitalGjeld"],
      );
}

class CompanyAccountingBrreg {
  Regnskapsperiode regnskapsperiode;
  EgenkapitalGjeld egenkapitalGjeld;
  String valuta;

  CompanyAccountingBrreg({
    required this.regnskapsperiode,
    required this.egenkapitalGjeld,
    required this.valuta,
  });

  factory CompanyAccountingBrreg.fromJson(dynamic json) {
    return CompanyAccountingBrreg(
      regnskapsperiode: Regnskapsperiode.fromJson(json['regnskapsperiode']),
      egenkapitalGjeld: EgenkapitalGjeld.fromJson(json['egenkapitalGjeld']),
      valuta: json['valuta'],
    );
  }

  static List<CompanyAccountingBrreg> companyFromSnapshot(List snapshot) {
    return snapshot.map((data) {
      return CompanyAccountingBrreg.fromJson(data);
    }).toList();
  }
}
