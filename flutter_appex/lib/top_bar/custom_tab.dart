import 'package:flutter/material.dart';

class CustomTab extends StatelessWidget {
  //CustomTab({required this.title});
  const CustomTab({Key? key, required this.iconPic}) : super(key: key);

  //final String title;
  final Icon iconPic;

  @override
  Widget build(BuildContext context) {
    //return Tab(child: Text(this.title, style: TextStyle(fontSize: 16)));
    return Tab(icon: iconPic);
  }
}
