import 'package:flutter/material.dart';

class CustomTabBar extends StatelessWidget {
  const CustomTabBar({Key? key, required this.controller, required this.tabs})
      : super(key: key);

  final TabController controller;
  final List<Widget> tabs;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double tabBarScaling = screenWidth > 1400
        ? 0.21
        : screenWidth > 1100
            ? 0.3
            : 0.4;
    // the smaller screen width the wider  the tapbar becomes
    return SizedBox(
      width: screenWidth * tabBarScaling,
      child: TabBar(
        controller: controller,
        tabs: tabs,
      ),
    );
  }
}
