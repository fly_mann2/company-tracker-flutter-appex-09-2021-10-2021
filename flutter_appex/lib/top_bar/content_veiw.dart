import 'package:flutter/material.dart';

import 'custom_tab.dart';

class ContentView {
  ContentView({required this.tab, required this.content});

  final CustomTab tab;
  // Can be whatever widget we want
  final Widget content;
}
