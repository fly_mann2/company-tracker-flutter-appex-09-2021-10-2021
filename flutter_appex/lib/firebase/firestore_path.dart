import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_demo/api/company_brreg_json.dart';
import 'package:flutter_demo/api/company_json.dart';

// Read/Write to firebase
class CompanyFirebase {
  String organisasjonsnummer;
  String navn;
  bool heart = false;
  String notes = "";
  CompanyFirebase({
    required this.organisasjonsnummer,
    required this.navn,
    required this.heart,
    required this.notes,
  });
}

class FirestorePath {
  // Access to update/setState function for front/favorite page.
  static Function? frontPageSetState;
  static Function? favoritePageSetState;

  static bool isLoggedIn() {
    return !(FirebaseAuth.instance.currentUser == null);
  }

  // Update FrontPage
  static void updateFrontPage() {
    if (frontPageSetState != null) {
      frontPageSetState!();
    }
  }

  // Update FavoritePage
  static void updateFavoritePage() {
    if (favoritePageSetState != null) {
      favoritePageSetState!();
    }
  }

  // Write to firebase
  static Future<void> setCompany(Company _company) async {
    if (isLoggedIn()) {
      FirebaseFirestore.instance
          .collection("Favorites_" + FirebaseAuth.instance.currentUser!.uid)
          .doc(_company.companyBrreg.organisasjonsnummer
              .toString()) // example company for now
          .set({
        "organisasjonsnummer": _company.companyBrreg.organisasjonsnummer,
        "navn": _company.companyBrreg.navn,
        "heart": _company.heart,
        "notes": _company.notes,
      });
    }
  }

  // Write Favorite to Firebase
  static Future<void> setFavorite(Company _company) async {
    if (isLoggedIn()) {
      setCompany(_company);
    }
  }

  // Write Notes to Firebase
  static Future<void> setNotes(Company _company) async {
    if (isLoggedIn()) {
      setCompany(_company);
    }
  }

  /* 
  _companys: List of companyes in frontpage.
  Get all companyes from Firebase.
  Check if any they overlap, if so.
    Add heart and notes to _companys.
  */
  static Future<void> getFrontPage(List<Company> _companys) async {
    List<CompanyFirebase> _companysTmp = await getAllFromFB();
    for (var i in _companys) {
      var contain = _companysTmp.where((element) =>
          element.organisasjonsnummer == i.companyBrreg.organisasjonsnummer);
      if (contain.length == 1) {
        CompanyFirebase tmp = _companysTmp.firstWhere((element) =>
            element.organisasjonsnummer == i.companyBrreg.organisasjonsnummer);
        i.heart = tmp.heart;
        i.notes = tmp.notes;
      }
    }
  }

  // Get companyes from Firebase for Favorites Page
  static Future getAllFavorites(List<Company> _companys) async {
    List<CompanyFirebase> _companysTmp = await getAllFromFB();

    for (var i in _companysTmp) {
      (i.heart == true)
          ? {
              _companys.add(Company(
                  companyBrreg: CompanyBrreg(
                      organisasjonsnummer: i.organisasjonsnummer,
                      navn: i.navn,
                      poststed: "",
                      postnummer: ""),
                  heart: i.heart,
                  notes: i.notes))
            }
          : /* nothing*/ {};
    }
  }

  // Title headers for list.
  static Company addEmptyCompany() {
    return Company(
        companyBrreg: CompanyBrreg(
          navn: "Navn:",
          organisasjonsnummer: "Org.NR:",
          poststed: "PostSted",
          postnummer: "NR:",
        ),
        heart: false,
        notes: "");
  }

  // Get companyes from firebase.
  static Future<List<CompanyFirebase>> getAllFromFB() async {
    List<CompanyFirebase> _companysFB = [];

    if (isLoggedIn()) {
      // Check if Firebase.collection exist
      final test = await FirebaseFirestore.instance
          .collection("Favorites_" + FirebaseAuth.instance.currentUser!.uid)
          .get();
      if (test.size > 0) {
        await FirebaseFirestore.instance
            .collection("Favorites_" + FirebaseAuth.instance.currentUser!.uid)
            .get()
            .then((snapshot) => {
                  for (var document in snapshot.docs)
                    {
                      _companysFB.add(CompanyFirebase(
                          organisasjonsnummer:
                              document.data()["organisasjonsnummer"],
                          navn: document.data()["navn"],
                          heart: document.data()["heart"],
                          notes: document.data()["notes"]))
                    }
                })
            .catchError((onError) {
          debugPrint(onError);
        });
      }
    }
    return _companysFB;
  }
}
